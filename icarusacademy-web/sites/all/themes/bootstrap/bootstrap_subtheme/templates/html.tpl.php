<?php
/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or
 *   'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see bootstrap_preprocess_html()
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 *
 * @ingroup themeable
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces;?>>
<head profile="<?php print $grddl_profile; ?>">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <?php print $scripts; ?>

  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
   <!-- ****** faviconit.com favicons ****** -->
    <link rel="shortcut icon" href="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon.ico">
    <link rel="icon" sizes="16x16 32x32 64x64" href=“/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon.ico">
    <link rel="icon" type="image/png" sizes="196x196" href=“/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon-192.png">
    <link rel="icon" type="image/png" sizes="160x160" href="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon-160.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon-96.png">
    <link rel="icon" type="image/png" sizes="64x64" href="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon-64.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon-32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon-16.png">
    <link rel="apple-touch-icon" href="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon-57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon-114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon-72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon-144.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon-60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon-120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon-76.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon-152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon-180.png">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/favicon-144.png">
    <meta name="msapplication-config" content="/sites/all/themes/bootstrap/bootstrap_subtheme/faviconit/browserconfig.xml">
    <!-- ****** faviconit.com favicons ****** -->
 <script src="//use.typekit.net/mgs5nog.js"></script>
<script>try{Typekit.load();}catch(e){}</script>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
