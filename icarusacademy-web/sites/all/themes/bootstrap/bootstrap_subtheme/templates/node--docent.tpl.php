  

  <div class="docent-wrapper row">
    <div class="col-sm-12 col-md-3">
    <?php print render($content['field_foto_docent']); ?> 
  </div>
  <div class="col-sm-12 col-md-9">
    <strong class="docent-name"> <?php print $title;?> </strong>
    <?php print render($content['field_info_docent']); ?>
  </div>
</div>
