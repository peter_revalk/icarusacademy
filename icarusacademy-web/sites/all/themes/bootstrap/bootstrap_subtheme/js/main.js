$(function() {
    $("img.lazy").lazyload({

            effect: "show",
            placeholder: "/sites/all/themes/fe_bartik/images/placeholder.gif"
        }

    );
    var maxSeats = parseInt($('.js-max-seats').text());
    var seatsTaken = parseInt($('.js-seats-taken').text());
    if (seatsTaken > maxSeats) {
        $('.webform-component--status-inschrijving input').val('Reserve lijst');
        console.log(maxSeats + ' ' + seatsTaken);
    }


    // querystring handler
    (function($) {
        $.QueryString = (function(a) {
            if (a == "") return {};
            var b = {};
            for (var i = 0; i < a.length; ++i) {
                var p = a[i].split('=');
                if (p.length != 2) continue;
                b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
            }
            return b;
        })(window.location.search.substr(1).split('&'))
    })(jQuery);


    //copy name of cursus when its soldout, and put it in the click thru link, 

    var cursusOrigin = $('.page-header').text() || '';
    var originalLink, newLink;
    //console.log(cursusOrigin);

    originalLink=$('.js-availabilities a').attr('href') || '';
    newLink = originalLink + '?cid=' + cursusOrigin;
    //console.log(newLink);
    $('.js-availabilities a').attr('href', newLink);
    
    //console.log(window.location.pathname + ' ');

    cursusMissed = $.QueryString["cid"] || '';
    if($('.webform-component--interesse-in-cursus textarea').length){
      $('.webform-component--interesse-in-cursus textarea').val(cursusMissed );
    }

   

    



});