"use strict";

var gulp = require('gulp');
var _ = require('lodash');

var gutil = require('gulp-util');
var sass = require('gulp-ruby-sass');

var debug = require('gulp-debug');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');

var rjs = require('gulp-requirejs');

var karma = require('karma').server;
//var mocha = require('karma-mocha');

var safari = require('karma-safari-launcher');
var ie = require('karma-ie-launcher');
var firefox = require('karma-firefox-launcher');
var opera = require('karma-opera-launcher');

var csslint = require('gulp-csslint');
var fs = require('fs');

var buildDir = 'sites/all/themes/';

/*
** File sources
*/
var sources = {
    scss: buildDir + '/**/*.scss',
    css:  buildDir + '/**/*.css',
    toWatch: {
        scss: buildDir + '/**/*.scss',
        css:  buildDir + '/**/*.css'
    },
    dest: buildDir
};

/*
** JS unit test files
    one could also externalize common config into a separate file,
    ex.: var karmaCommonConf = require('./karma-common-conf.js');
*/
var karmaCommonConf = {
  // base path that will be used to resolve all patterns (eg. files, exclude)
  basePath: '',
  // browsers: ['Chrome', 'Safari', 'Firefox', 'PhantomJS', 'IE'],
  browsers: ['Chrome'],
  // browsers: ['PhantomJS'],
  frameworks: ['requirejs','mocha', 'chai'],
  files: [
    {pattern:  buildDir + 'dpp_base_theme/js/lib/**/*.js', included: false},
    {pattern:  buildDir + 'dpp_base_theme/js/**/*.js', included: false},
    {pattern:  buildDir + 'dpp_base_theme/test/**/*.js', included: false},
    {pattern: 'karma-require.js', included: true},
    {pattern: 'http://test10g.caps.demorgen.be.persgroep.be/service/ssologin/ssoconnect.js', included: true}
  ],
  exclude: [
       /*buildDir + 'dpp_base_theme/js/dppplus.js',
       buildDir + 'dpp_base_theme/js/app/angular/app.js'*/
   ],
   reporters: ['progress']

};

/*
** Messages
*/
var messages = {
    scss: {
        begin: "SCSS task has begun",
        success: "SCSS compiled successfully"
    }
}

/*
** Define tasks
*/
var stats = {},
    begin,
    beginString,
    end,
    endString;

var tasks = {
    scss: function () {
        gutil.log( gutil.colors.bgMagenta( messages.scss.begin ) )
        gulp.src( sources.scss )
            .pipe( sass( {compass: true, 'sourcemap=none': true, require: ['susy','modular-scale']} ) )
            .on( 'error', function (err) { console.log(err.message); } )
            .pipe( gulp.dest( buildDir ) );
    }
};

//TODO: migrate r.js build...
gulp.task('requirejsBuild', function() {
    rjs({
        baseUrl: 'path/to/your/base/file.js',
        out: 'FILENAME\_TO\_BE\_OUTPUTTED',
        shim: {
            // standard require.js shim options
        },
        // ... more require.js options
    })
        .pipe(gulp.dest('./deploy/')); // pipe it to the output DIR
});

/*
 ** Register tasks
 */

gulp.task('build:scss', tasks.scss);

gulp.task('dev', ['build:scss', 'watch']);

/**
 * JS HINT
 */
gulp.task('hint', function() {
  return gulp.src( [
    buildDir + '**/*.js',
    '!**/lib/**/*',
    '!**/polyfills/**/*',
    '!**/js-src-build/**/*',
    '!**/colorpicker.js',
    '!**/rubik/**/*.js',
     ] )
    .pipe(jshint())
    .pipe(jshint.reporter( stylish ));
});

/**
 * CSS LINT
 */
gulp.task( 'lint', function() {
    return gulp.src( sources.css )
        .pipe( csslint() )
        .pipe( csslint.reporter(toFileReporter) );
});

var toFileReporter = function( file ) {
    var opts = {},
        wrStream,
        out = [],
        results = file.csslint.results

    opts.filename =  './logs/' + file.path.replace(/^.*[\\\/]/, '') + '.log';

    if (wrStream && filename !== opts.filename) {
        wrStream.end();
        wrStream = null;
    }

    if (!wrStream) {
        wrStream = fs.createWriteStream(opts.filename);
    }

    var out = [];

    results.forEach(function (result, i) {
        var err = result.error;

        if (i === 0) {
            // start off the output with the filename
            out.push(results.length + ' lint errors found in ' + result.file);
        }

        out.push('  [' + err.line + ',' + err.col + '] ' + err.message);
    });

    wrStream.write(out.join('\n'));
};

/**
 * Run test once and exit
 */
gulp.task('test', function (done) {
  karma.start(_.assign({}, karmaCommonConf, {singleRun: true}), done);
});

/**
 * Watch for file changes and re-run tests on each change
 */
gulp.task('tdd', function (done) {
  karma.start(karmaCommonConf, done);
});

/**
 * Watch for changes
 */
gulp.task('watch', function () {
    // watch scss files
    gulp.watch(sources.toWatch.scss, ['build:scss'])
    .on('change', function( event ) {
        console.log(gutil.colors.cyan('[SCSS Watch] File ' + event.path.replace(/.*(?=sass)/,'') + ' was ' + event.type + ', compiling...'))
    })
    gulp.watch( sources.toWatch.css, ['lint'] );
});
